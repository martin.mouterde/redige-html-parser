(function () {
    "use strict";

    const htmlparser = require("htmlparser2");

    const HTMLSaxHandler = require("./HTMLSaxHandler");
    const HTMLNormalize = require("./HTMLNormalize");
    const ToRedigeModel = require("./ToRedigeModel");

    function HTMLParser() {
    }

    HTMLParser.DefaultHTMLSaxHandler = HTMLSaxHandler;
    HTMLParser.DefaultHTMLNormalize = HTMLNormalize;

    HTMLParser.prototype.parse = function (str, opt) {
        let options = opt || {};
        options.saxHandler = options.saxHandler || new HTMLSaxHandler();
        options.nomalizer = options.nomalizer || new HTMLNormalize();

        let parser = new htmlparser.Parser(options.saxHandler, {
            decodeEntities: true,
            recognizeSelfClosing: true
        });
        parser.write(str);
        parser.end();

        let mapper = new ToRedigeModel();
        return mapper.convert(options.nomalizer.normalize(options.saxHandler.result));
    };

    module.exports = HTMLParser;
})();