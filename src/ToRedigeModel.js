(function () {
    "use strict";

    const _ = require("lodash");

    function ToRedigeModel() {
    }

    ToRedigeModel.prototype.convert = function (root) {
        const doc = {children: []};
        root.children.forEach(function (section) {
            doc.children.push(mapSection(section));
        });
        if (root.copyType === 'inline') {
            return {content: doc.children[0], copyType: root.copyType};
        } else {
            return {content: doc, copyType: root.copyType};
        }
    };

    function mapSection(section) {
        const result = {};
        if (isHierarchical(section)) {
            trimSectionTree(section);
            result.type = "hierarchical";
            result.level = isHierarchicalLevel(section);
            result.content = getIncludedText(section);
            result.decorators = buildDecorators(section);
            removeFullStyledTitleDecorator(result);
        } else if (section.name === 'img') {
            result.type = "image";
            result.data = section.attr.src;
        } else if (section.name === 'pre') {
            result.type = "code";
            result.children = section.children.map(function (child) {
                return {type: "hierarchical", level: 0, decorators: [], content: getIncludedText(child)};
            });
        } else if (section.name === 'blockquote') {
            result.type = "blockquote";
            result.children = section.children.map(function (child) {
                return mapSection(child);
            });
        } else if (section.name.indexOf('list') === 1) {
            trimSectionTree(section);
            result.type = section.name;
            result.level = section.level;
            result.content = getIncludedText(section);
            result.decorators = buildDecorators(section);
        }
        return result;
    }

    function trimSectionTree(node) {
        let start = getFirstTextNode(node);
        if (start) {
            start.text = _.trimStart(start.text);
        }

        let end = getLastTextNode(node);
        if (end) {
            end.text = _.trimEnd(end.text);
        }

    }

    function getFirstTextNode(node) {
        if (_.isEmpty(node.children)) {
            return node;
        } else if (!_.isEmpty(node.children)) {
            return getFirstTextNode(node.children[0]);
        }
    }

    function getLastTextNode(node) {
        if (_.isEmpty(node.children)) {
            return node;
        } else if (!_.isEmpty(node.children)) {
            return getLastTextNode(node.children[node.children.length - 1]);
        }
    }

    function buildDecorators(sectionNode) {
        let sectionContent = {text: "", decorators: []};
        sectionNode.children.forEach(function (inline) {
            flattenInline(inline, sectionContent);
        });
        return sectionContent.decorators;
    }

    function flattenInline(node, sectionContent) {
        let semantic = getDecoratorType(node);
        if (!_.isEmpty(semantic)) {
            let decorator = {
                start: sectionContent.text.length,
                end: sectionContent.text.length + getIncludedText(node).length,
                type: semantic
            };
            if (decorator.type === "link") {
                decorator.data = node.attr.href;
            }
            sectionContent.decorators.push(decorator);
        }

        if (_.isEmpty(node.children)) {
            sectionContent.text += node.text;
        } else {
            node.children.forEach(function (inline) {
                flattenInline(inline, sectionContent);
            });
        }
    }

    /*
     * Workd,Gdoc... could add title styles in their stylesheet, to avoid
     * getting title style this methode will remove decorators whitch decorate the full title section
     */
    function removeFullStyledTitleDecorator(section) {
        if (section.type === "hierarchical" && section.level > 0) {
            let decorators = _.filter(section.decorators, {start: 0, end: section.content.length});
            decorators.forEach(function (decorator) {
                if (decorator.type !== 'link') {
                    _.remove(section.decorators, decorator);
                }
            });
        }
    }

    function getDecoratorType(node) {
        if (isLink(node)) {
            return "link";
        }
        if (isBold(node)) {
            return "bold";
        }
        if (isItalic(node)) {
            return "italic";
        }
        if (isCode(node)) {
            return "code";
        }
    }

    function isBold(node) {
        return node.name === "b" || node.name === "strong" || (node.class && node.class.indexOf("bold") !== -1);
    }

    function isItalic(node) {
        return node.name === "i" || node.name === "em" || (node.class && node.class.indexOf("italic") !== -1);
    }

    function isLink(node) {
        return (node.name === "a" && node.attr.href ? node.attr.href : null) || (node.class && node.class.indexOf("link") !== -1);
    }

    function isCode(node) {
        return node.name === "code";
    }

    function getIncludedText(node) {
        let result = "";
        if (_.isEmpty(node.children)) {
            result = node.text;
        } else {
            node.children.forEach(function (subNode) {
                result += getIncludedText(subNode);
            });
        }
        return result;
    }

    function isHierarchical(section) {
        if (!section) {
            return false;
        }
        return ["p", "h1", "h2", "h3", "h4", "h5", "h6"].indexOf(section.name) !== -1;
    }

    function isHierarchicalLevel(section) {
        if (section.name === "p") {
            return 0;
        } else if (section.name.indexOf("h") === 0) {
            return parseInt(section.name.substring(1));
        }
    }

    module.exports = ToRedigeModel;
})();