(function () {
    "use strict";

    let expect = require('chai').expect;
    let fileToString = require('../FileToString');
    let HTMLParser = require('../../src/HTMLParser');

    describe('ide/pasteFromIDE.spec.js', function () {
        describe('range paste', function () {
            let result;
            before(function () {
                return fileToString.read("./test/ide/sample.html").then(function (content) {
                    let parser = new HTMLParser();
                    result = parser.parse(content).content;
                });
            });
            it('should retreive the char', function () {
                expect(result).to.deep.equal({
                    "children": [
                        {
                            "children": [
                                {
                                    "content": "describe('Simple Headers', function () {",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "    var result;",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "    before(function () {",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "        return fileToString.read(\"./test2/paste/gdoc/samples/headers.html\").then(function (content) {",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "            result = HTMLParser.parse(content).content;",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "        });",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "    });",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "    it('should retreive the char', function () {",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "        expect(_.get(result, 'children.length')).to.equal(9);",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "        //\"<h1><span>Titre 1</span></h1>\"",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "        expect(result.children[0]).to.deep.equal({",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "            content: \"Titre 1\",",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "            type: 'hierarchical',",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "            level: 1,",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "            decorators: []",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "        });",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "    });",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                                {
                                    "content": "});",
                                    "decorators": [],
                                    "level": 0,
                                    "type": "hierarchical",
                                },
                            ],
                            "type": "code",
                        },
                    ],
                });
            });
        });
    });
})();