(function () {
    "use strict";

    let expect = require('chai').expect;
    let fileToString = require('../FileToString');
    let HTMLParser = require('../../src/HTMLParser');

    describe('test/web1/pasteFromChrome.spec.js', function () {
        describe('range paste', function () {
            let result;
            before(function () {
                //from https://www.w3.org/wiki/HTML_lists
                return fileToString.read("./test/web1/sample.html").then(function (content) {
                    let parser = new HTMLParser();
                    result = parser.parse(content).content;
                });
            });
            it('should retreive the char', function () {
                expect(result).to.deep.equal({
                    "children": [
                        {
                            "content": "Choosing between list types",
                            "decorators": [],
                            "level": 2,
                            "type": "hierarchical",
                        },
                        {
                            "content": "When trying to decide what type of list to use, you can usually decide by asking two simple questions:",
                            "decorators": [],
                            "level": 0,
                            "type": "hierarchical",
                        },
                        {
                            "content": "Am I defining terms or associating other name/value pairs?",
                            "decorators": [],
                            "level": 0,
                            "type": "olist",
                        },
                        {
                            "content": "If yes, use a description list.",
                            "decorators": [],
                            "level": 1,
                            "type": "ulist",
                        },
                        {
                            "content": "If no, don’t use a description list — go on to the next question.",
                            "decorators": [],
                            "level": 1,
                            "type": "ulist",
                        },
                        {
                            "content": "Is the order of the list items important?",
                            "decorators": [],
                            "level": 0,
                            "type": "olist",
                        },
                        {
                            "content": "If yes, use an ordered list.",
                            "decorators": [],
                            "level": 1,
                            "type": "ulist",
                        },
                        {
                            "content": "If no, use an unordered list.",
                            "decorators": [],
                            "level": 1,
                            "type": "ulist",
                        },
                        {
                            "content": "The difference between HTML lists and text",
                            "decorators": [],
                            "level": 2,
                            "type": "hierarchical",
                        },
                        {
                            "content": "You may be wondering what the difference is between an HTML list and some text with bullets or numbers written in by hand. Well, there are several advantages to using an HTML list:",
                            "decorators": [],
                            "level": 0,
                            "type": "hierarchical",
                        },
                        {
                            "content": "If you have to change the order of the list items in an ordered list, you simply move around the list item elements. If you wrote the numbers in manually you would have to go through and change every single item’s number to correct the order — which is tedious to say the least!",
                            "decorators": [],
                            "level": 0,
                            "type": "ulist",
                        },
                        {
                            "content": "Using an HTML list allows you to style the list properly - you can use CSS to style just the list elements. If you just use a blob of text, you will find it more difficult to style the individual items in any useful manner, as the elements used will be the same as used for every other piece of text.",
                            "decorators": [],
                            "level": 0,
                            "type": "ulist",
                        },
                        {
                            "content": "Using an HTML list gives the content the proper semantic structure, as well as a \"list-ish\" visual effect. This has important benefits such as allowing screen readers to tell users with visual impairments they are reading a list, rather than just reading out a confusing jumble of text and numbers.",
                            "decorators": [],
                            "level": 0,
                            "type": "ulist",
                        },
                        {
                            "content": "To put it another way: text and lists are not the same. Using text instead of a list makes more work for you and can create problems for your document’s readers. So if your document needs a list, you should use the correct HTML list.",
                            "decorators": [
                                {
                                    "end": 54,
                                    "start": 23,
                                    "type": "bold",
                                },
                            ],
                            "level": 0,
                            "type": "hierarchical",
                        },
                        {
                            "content": "",
                            "decorators": [],
                            "level": 0,
                            "type": "hierarchical",
                        },
                    ]
                });
            });
        });
    });
})();