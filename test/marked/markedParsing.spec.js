(function () {
    "use strict";

    let expect = require('chai').expect;
    let fileToString = require('../FileToString');
    let HTMLParser = require('../../src/HTMLParser');

    describe('test/marked/markedParsing.spec.js', function () {
        let result;
        before(function () {
            //from https://www.w3.org/wiki/HTML_lists
            return fileToString.read("./test/marked/markedParsing.html").then(function (content) {
                let parser = new HTMLParser();
                result = parser.parse(content).content;
            });
        });
        it('should retreive the char', function () {
            expect(result).to.deep.equal({
                "children": [
                    {
                        "content": "Mon Titre 1",
                        "decorators": [],
                        "level": 1,
                        "type": "hierarchical"
                    },
                    {
                        "content": "Mon Titre 2",
                        "decorators": [],
                        "level": 2,
                        "type": "hierarchical",
                    },
                    {
                        "content": "Mon Titre 3",
                        "decorators": [],
                        "level": 3,
                        "type": "hierarchical"
                    },
                    {
                        "content": "Mon Titre 5",
                        "decorators": [],
                        "level": 5,
                        "type": "hierarchical"
                    },
                    {
                        "content": "Mon Titre 6",
                        "decorators": [],
                        "level": 6,
                        "type": "hierarchical",
                    },
                    {
                        "children": [
                            {
                                "content": "aaa\nBonjour** voici un [élé**mént](http://demo.redige.net) décoré\nfunction(){\nvar toto = 4;\nconsole.log('hey !!');\n//nop\n",
                                "decorators": [],
                                "level": 0,
                                "type": "hierarchical",
                            },
                        ],
                        "type": "code",
                    },
                    {
                        "children": [
                            {
                                "content": "mon texte 1",
                                "decorators": [],
                                "level": 0,
                                "type": "hierarchical",
                            },
                            {
                                "content": "You think water moves fast? You should see ice. It moves like it has a mind. Like it knows it killed the world once and got a taste for murder. After the avalanche, it took us a week to climb out. Now, I don't know exactly when we turned on each other, but I know that seven of us survived the slide... and only five made it out. Now we took an oath, that I'm breaking now. We said we'd say it was the snow that killed the other two, but it wasn't. Nature is lethal but it doesn't hold a candle to man.",
                                "decorators": [
                                    {
                                        "end": 20,
                                        "start": 7,
                                        "type": "bold",
                                    },
                                    {
                                        "data": "http://www.google.com",
                                        "end": 214,
                                        "start": 210,
                                        "type": "link",
                                    },
                                ],
                                "level": 0,
                                "type": "hierarchical",
                            },
                            {
                                "content": "",
                                "decorators": [],
                                "level": 0,
                                "type": "hierarchical",
                            }
                        ],
                        "type": "blockquote",
                    },
                    {
                        "content": "mon texte 1",
                        "decorators": [],
                        "level": 0,
                        "type": "hierarchical",
                    },
                    {
                        "content": "You think water moves fast? You should see ice. It moves like it has a mind. Like it knows it killed the world once and got a taste for murder. After the undefinedavalanche, var function(){} it took us a week tundefinedo climb out. Now, I don't know exactly when we turned on each other, but I know that seven of us survived the slide... and only five made it out. Now we took an oath, that I'm breaking now. We said we'd say it was the snow that killed the other two, but it wasn't. Nature is lethal but it doesn't hold a candle to man.",
                        "decorators": [
                            {
                                "end": 20,
                                "start": 7,
                                "type": "bold",
                            },
                            {
                                "data": "http://www.google.com",
                                "end": 249,
                                "start": 245,
                                "type": "link",
                            },
                        ],
                        "level": 0,
                        "type": "hierarchical",
                    },
                    {
                        "content": "Mon TExt 2",
                        "decorators": [],
                        "level": 0,
                        "type": "hierarchical",
                    },
                    {
                        "data": "http://soocurious.com/fr/wp-content/uploads/2014/07/personne-redige-rapport.jpg",
                        "type": "image",
                    },
                    {
                        "content": "",
                        "decorators": [],
                        "level": 0,
                        "type": "hierarchical",
                    },
                    {
                        "content": "Mon TExt 1",
                        "decorators": [],
                        "level": 0,
                        "type": "olist",
                    },
                    {
                        "content": "Mon TExt 2",
                        "decorators": [],
                        "level": 0,
                        "type": "ulist",
                    },
                ]
            });
        });
    });
})();